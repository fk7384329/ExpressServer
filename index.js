const express = require('express');
const mongoose = require('mongoose');

const app = express();

// Middleware to parse JSON request bodies
app.use(express.json());

// Connect to the remote MongoDB database
mongoose.connect('mongodb+srv://tomajih182:J18JtCLaMQUUEp1l@expressservercluster.6wqo1bl.mongodb.net/?retryWrites=true&w=majority', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
})
  .then(() => {
    console.log('Connected to MongoDB');

    // Start the server after successful database connection
    app.listen(3000, () => {
      console.log('Server is running on port 3000');
    });
  })
  .catch((error) => {
    console.error('Error connecting to MongoDB:', error);
  });

// Define a schema and model for your data
const DataModel = mongoose.model('Data', {
  title: String,
  content: String,
});

// POST request route
app.post('/data', async (req, res) => {
  try {
    const { title, content } = req.body;

    // Create a new data entry in the database
    const newData = await DataModel.create({
      title,
      content,
    });

    res.status(201).json(newData);
  } catch (error) {
    console.error('Error creating data:', error);
    res.status(500).json({ error: 'An error occurred' });
  }
});

app.get('/', (req, res) => {
  res.send('Hello, world!');
});
